# Generated by Django 3.0.7 on 2020-06-24 04:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('farmacia', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
            ],
        ),
        migrations.AlterField(
            model_name='farmaco',
            name='contenido',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='farmaco',
            name='marca',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='farmacos', to='farmacia.Marca'),
        ),
    ]
