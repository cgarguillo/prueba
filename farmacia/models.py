from django.db import models

# Create your models here.

class Marca(models.Model):                     #otra
   nombre = models.CharField(max_length=200)

   class Meta:
      ordering = ["nombre"]

class Farmaco(models.Model):
   nombre = models.CharField(max_length=200)
   contenido = models.IntegerField(blank=True, null=True)
   #  related_name sirve para poder definir el como se va a llamar desde se clase relacionada,
   #  por ejemplo en este caso con marca.farmacos.all() se va a poder obtener todas las marcas
   marca = models.ForeignKey(Marca, on_delete=models.CASCADE, related_name='farmacos', null=True)

   class Meta:
      ordering = ["nombre"]
