from django.shortcuts import render, redirect
from farmacia.models import Farmaco, Marca
from farmacia.helpers.cargas import cargar_droga_basic, cargar_marca_basic, modificar_droga_basic
from django.db import IntegrityError
# Create your views here.

def cargar_marca(request):
    lista_marca = Marca.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            cargar_marca_basic(nombre)
        except Exception as err:
            return render(request, 'marca_carga.html', {'lista_marca': lista_marca,'error': str(err)})
    return render(request, 'marca_carga.html', {'lista_marca': lista_marca})


def cargar_droga(request):
    lista_droga = Farmaco.objects.all()
    lista_marca = Marca.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            contenido = request.POST.get('contenido')
            marca_id = request.POST.get('marca_id')
            cargar_droga_basic(nombre,contenido,marca_id)
        except IntegrityError as otroErr:
            err = 'HAY UN ERROR BD EN LA PAGINA'
            return render(request, 'lista_droga.html',{'lista_droga': lista_droga, 'lista_marca': lista_marca, 'error': str(otroErr)})
        except Exception as err:
            return render(request, 'lista_droga.html', {'lista_droga': lista_droga, 'lista_marca': lista_marca,'error':str(err)})
    return render(request, 'lista_droga.html', {'lista_droga': lista_droga, 'lista_marca': lista_marca})



def modificar_droga(request, id_droga):
    droga = Farmaco.objects.get(id=id_droga)
    lista_marca = Marca.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')        ##() va la variable que ingresamos en el html
            contenido = request.POST.get('contenido')  ##() va la variable que ingresamos en el html
            marca_id = request.POST.get('marca_id')
            modificar_droga_basic(droga,nombre,contenido,marca_id) # se agrega droga

        except IntegrityError as otroErr:
            err = 'ErrorBD'
            return render(request, 'modificar_droga.html', {'droga': droga, 'lista_marca': lista_marca,'error': str(otroErr)})

        except Exception as err:
            return render(request, 'modificar_droga.html',{'droga': droga, 'lista_marca': lista_marca, 'error': str(err)})

        return redirect('/farmaco/cargar_droga')

    return render(request, 'modificar_droga.html', {'droga': droga, 'lista_marca': lista_marca})








