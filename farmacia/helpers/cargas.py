from farmacia.models import Marca, Farmaco     #importamos los models


def cargar_marca_basic(nombre):
    lista_marca = Marca.objects.all()

    for item in lista_marca:
        if item.nombre == nombre:
            raise Exception("Alerta!!!")

    marca = Marca()  # creammos instancias el objeto
    marca.nombre = nombre
    marca.save()  #Se guarda en la BD

def cargar_droga_basic(nombre, contenido, marca_id):
    lista_droga = Farmaco.objects.all()

    for item in lista_droga:
        if item.nombre == nombre and item.contenido == int(contenido) and item.marca_id == int(marca_id):
            raise Exception("Atencion!!!")

    droga = Farmaco()
    droga.nombre = nombre
    droga.contenido = contenido
    droga.marca_id = marca_id
    droga.save()


def modificar_droga_basic(droga, nombre, contenido, marca_id):  ###############################
    lista_droga = Farmaco.objects.all()

    for item in lista_droga:
        if item.nombre == nombre and item.contenido == int(contenido) and item.marca_id == int(marca_id):
            raise Exception("No se puede duplicar Droga y Contenido")

    droga.nombre = nombre
    droga.contenido = contenido
    droga.marca_id = marca_id
    droga.save()
